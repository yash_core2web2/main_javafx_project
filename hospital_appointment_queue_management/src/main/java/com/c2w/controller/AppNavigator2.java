package com.c2w.controller;

import com.c2w.hospital_dashboard.Doctors;
import com.c2w.hospital_dashboard.Information;
import com.c2w.hospital_dashboard.Pending;
import com.c2w.hospital_dashboard.Previous;
import com.c2w.patient_dashboard.Book_New_Appointment;
import com.c2w.patient_dashboard.Previous_Appointments;
import com.c2w.patient_dashboard.Upcoming_Appointments;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppNavigator2 extends Application {
    private Stage primaryStage;
    private Scene infoScene,previous1Scene,pendingScene;
    private Scene doctorScene;

    private Information pageInfo;
    private Previous pagePreviuos1;
    private Pending pagePending;
    private Doctors pageList;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        pageInfo = new Information(this);
        pagePreviuos1 = new Previous(this);
        pagePending = new Pending(this);
        pageList = new Doctors(this);

        previous1Scene = new Scene(pagePreviuos1.getView(),1920,1080);
        pendingScene = new Scene(pagePending.getView(),1920,1080);
        infoScene = new Scene(pageInfo.getView(),1920,1080);
        doctorScene = new Scene(pageList.getView(),1920,1080);

        primaryStage.setScene(infoScene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.show();
    }



        public void NavigateToInfo() {
            primaryStage.setScene(infoScene);
        }
        
    
    
        public void NavigateToPending() {
            primaryStage.setScene(pendingScene);
        }
    
        public void NavigateToPrevious1() {
            primaryStage.setScene(previous1Scene);
        }

        public void NavigateToDoctors() {
            primaryStage.setScene(doctorScene);
        }

        
        
    
    
}
