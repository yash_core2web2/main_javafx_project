package com.c2w.controller;

import com.c2w.patient_dashboard.Book_New_Appointment;
import com.c2w.patient_dashboard.Previous_Appointments;
import com.c2w.patient_dashboard.Upcoming_Appointments;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppNavigation extends Application{

    private Stage primaryStage;
    private Scene book_newScene,previousScene,upcomingScene;

    private Book_New_Appointment pageBook;
    private Previous_Appointments pagePreviuos;
    private Upcoming_Appointments pageUpcoming;





    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        pageBook = new Book_New_Appointment(this);
        pagePreviuos = new Previous_Appointments(this);
        pageUpcoming = new Upcoming_Appointments(this);

        book_newScene = new Scene(pageBook.getView(),1920,1080);
        previousScene = new Scene(pagePreviuos.getView(),1920,1080);
        upcomingScene = new Scene(pageUpcoming.getView(),1920,1080);

        primaryStage.setScene(book_newScene);
        primaryStage.setMaximized(true);
        primaryStage.setResizable(false);
        primaryStage.show();

        
        
    }


    public void NavigateToBook_new() {
        primaryStage.setScene(book_newScene);
    }


    public void NavigateToUpcoming() {
        primaryStage.setScene(upcomingScene);
    }

    public void NavigateToPrevious() {
        primaryStage.setScene(previousScene);
    }
    
}
