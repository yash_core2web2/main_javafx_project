package com.c2w.hospital_dashboard;

import java.time.LocalDate;

import com.c2w.controller.AppNavigation;
import com.c2w.controller.AppNavigator2;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import javafx.stage.Stage;

public class Information {

    private AppNavigator2 app1;
    private Pane view;
    Image bgImage;
    BackgroundImage backgroundImage ;
    Button information_button;
    Button pending_appointment;
    Button completed_appointment;
    Label welcome;
    Button list_doctor;
    Label sit_cap;
    Label appointment_cap;
    TextField sitTField;
    TextField appointmentTField;
    Button logout;
    Button about_us;

    public Information(AppNavigator2 app1) {
        this.app1 = app1;
        initialize();
    }
    public Pane getView() {
        return view;
    }

    public void initialize() {
       welcome = new Label("Welcome to Navale Hospital");
       welcome.setLayoutY(15);
       welcome.setLayoutX(5);
       welcome.setFont(new Font("Cooper Black", 55));
       welcome.setTextFill(Color.ROSYBROWN);

       HBox welcomeHBox = new HBox(welcome);
       welcomeHBox.setPrefWidth(1920);
       welcomeHBox.setAlignment(Pos.CENTER);

       bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);
       
       information_button = new Button("Information");
       pending_appointment = new Button("Pending Appointments");
       completed_appointment = new Button("Completed Appointments");
       information_button.setPrefWidth(640);
       pending_appointment.setPrefWidth(640);
       completed_appointment.setPrefWidth(640);
       information_button.setPrefHeight(55);
       pending_appointment.setPrefHeight(55);
       completed_appointment.setPrefHeight(55);
       information_button.setStyle("-fx-font-weight:bold");
       pending_appointment.setStyle(" -fx-font-weight:bold");
       completed_appointment.setStyle("-fx-font-weight:bold");

       completed_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app1.NavigateToPrevious1();
        }
       });
       pending_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app1.NavigateToPending();
        }
       });
       
       HBox hBox = new HBox(information_button,pending_appointment,completed_appointment);
       hBox.setLayoutY(993);

       DatePicker datePicker = new DatePicker();
       datePicker.setValue(LocalDate.now());
       datePicker.setPrefWidth(175);
       datePicker.setPrefHeight(35);
       datePicker.setStyle("-fx-font-weight:bold");

//********************************************************************************************************** */
       TextField timeField = new TextField();
        timeField.setPromptText("HH:MM AM/PM");
        timeField.setEditable(false);
        Button openButton = new Button("Select Opening Time");
        ComboBox<Integer> hourComboBox = new ComboBox<>();
        for (int i = 1; i <= 12; i++) {
            hourComboBox.getItems().add(i);
        }
        ComboBox<Integer> minuteComboBox = new ComboBox<>();
        for (int i = 0; i < 60; i++) {
            minuteComboBox.getItems().add(i);
        }
        ComboBox<String> amPmComboBox = new ComboBox<>();
        amPmComboBox.getItems().addAll("AM", "PM");
        amPmComboBox.setValue("AM");

        HBox timePickerBox = new HBox(10, hourComboBox, new Label(":"), minuteComboBox, amPmComboBox);
        timePickerBox.setLayoutY(115);
        timePickerBox.setLayoutX(130);
    

        Popup popup = new Popup();
        popup.getContent().add(timePickerBox);
        popup.setAutoHide(true);

        openButton.setOnAction(e -> {
            if (!popup.isShowing()) {
                popup.show(view.getScene().getWindow()); 
            }
        });


        ChangeListener<Object> timeChangeListener = (observable, oldValue, newValue) -> {
            if (hourComboBox.getValue() != null && minuteComboBox.getValue() != null && amPmComboBox.getValue() != null) {
                int hour = hourComboBox.getValue();
                int minute = minuteComboBox.getValue();
                String amPm = amPmComboBox.getValue();
                String formattedTime = String.format("%02d:%02d %s", hour, minute, amPm);
                timeField.setText(formattedTime);

                popup.hide(); 
            }
        };

        hourComboBox.valueProperty().addListener(timeChangeListener);
        minuteComboBox.valueProperty().addListener(timeChangeListener);
        amPmComboBox.valueProperty().addListener(timeChangeListener);

        VBox root = new VBox(10, timeField, openButton);
        root.setAlignment(Pos.CENTER);

//*********************************************************************************************************** */
        TextField timeField1 = new TextField();
        timeField1.setPromptText("HH:MM AM/PM");
        timeField1.setEditable(false);
        Button openButton1 = new Button("Select Closing Time");
        ComboBox<Integer> hourComboBox1 = new ComboBox<>();
        for (int i = 1; i <= 12; i++) {
            hourComboBox1.getItems().add(i);
        }
        ComboBox<Integer> minuteComboBox1 = new ComboBox<>();
        for (int i = 0; i < 60; i++) {
            minuteComboBox1.getItems().add(i);
        }
        ComboBox<String> amPmComboBox1 = new ComboBox<>();
        amPmComboBox1.getItems().addAll("AM", "PM");
        amPmComboBox1.setValue("AM");

        HBox timePickerBox1 = new HBox(10, hourComboBox1, new Label(":"), minuteComboBox1, amPmComboBox1);
        timePickerBox1.setLayoutY(115);
        timePickerBox1.setLayoutX(910);
    

        Popup popup1 = new Popup();
        popup1.getContent().add(timePickerBox1);
        popup1.setAutoHide(true);

        openButton1.setOnAction(e -> {
            if (!popup1.isShowing()) {
                popup1.show(view.getScene().getWindow()); 
            }
        });


        ChangeListener<Object> timeChangeListener1 = (observable, oldValue, newValue) -> {
            if (hourComboBox1.getValue() != null && minuteComboBox1.getValue() != null && amPmComboBox1.getValue() != null) {
                int hour = hourComboBox1.getValue();
                int minute = minuteComboBox1.getValue();
                String amPm = amPmComboBox1.getValue();
                String formattedTime = String.format("%02d:%02d %s", hour, minute, amPm);
                timeField1.setText(formattedTime);

                popup1.hide(); 
            }
        };

        hourComboBox1.valueProperty().addListener(timeChangeListener1);
        minuteComboBox1.valueProperty().addListener(timeChangeListener1);
        amPmComboBox1.valueProperty().addListener(timeChangeListener1);

        VBox root1 = new VBox(10, timeField1, openButton1);
        root1.setAlignment(Pos.CENTER);

//*********************************************************************************************************** */
        HBox timehHBox = new HBox(200,root,root1);
        timehHBox.setPrefWidth(600);
        timehHBox.setAlignment(Pos.CENTER);

       VBox vBox = new VBox(40,datePicker,timehHBox);
       vBox.setPrefWidth(600);
       vBox.setAlignment(Pos.CENTER);
       vBox.setLayoutY(150);
       vBox.setLayoutX(900);

       sit_cap = new Label("Sitting Capacity");
       appointment_cap = new Label("Appointment Capacity of a Slot");
       sitTField = new TextField();
       appointmentTField = new TextField();
       HBox sitHBox = new HBox(115,sit_cap,sitTField);
       HBox appointmentHBox = new HBox(10,appointment_cap,appointmentTField);

       sitHBox.setPrefWidth(600);
       //sitHBox.setAlignment(Pos.CENTER);
       appointmentHBox.setPrefWidth(600);
       //appointmentHBox.setAlignment(Pos.CENTER);

       list_doctor = new Button("List Available Doctors Today");
       list_doctor.setPrefHeight(35);

       list_doctor.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            app1.NavigateToDoctors();
        }
       });
       VBox vb1 = new VBox(50,list_doctor,sitHBox,appointmentHBox);
       vb1.setAlignment(Pos.CENTER);

       VBox totalVBox = new VBox(80,vBox,vb1);
       totalVBox.setPrefWidth(600);
       totalVBox.setAlignment(Pos.CENTER);
       totalVBox.setLayoutY(225);
       totalVBox.setLayoutX(900);

       logout = new Button("Logout");
       logout.setPrefHeight(35);
       about_us = new Button("About Us");
       about_us.setPrefHeight(35);

       HBox outHBox = new HBox(5,about_us,logout);
       outHBox.setLayoutX(1750);
       outHBox.setLayoutY(850);
       
       view = new Pane(welcomeHBox,hBox,totalVBox,outHBox);
       view.setBackground(background);
}
}
