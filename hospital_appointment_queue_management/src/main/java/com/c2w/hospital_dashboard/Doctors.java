package com.c2w.hospital_dashboard;

import com.c2w.controller.AppNavigator2;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Doctors{
    private AppNavigator2 app1;
    private Pane view;
    Image bgImage;
    BackgroundImage backgroundImage ;
    String id;
    String name;
    String specialization;
    String degree;
    TableView<Doctors> table;
    TextField idinput,nameinput,spinput,degreeinput;
    Button add,delete;
    Button backButton;
    Image backImage;
    public Doctors(AppNavigator2 app1) {
        this.app1 = app1;
        initialize();
    }
    public Pane getView() {
        return view;
    }

    public Doctors(){
        this.id = "";
        this.name = "";
        this.specialization = "";
        this.degree = "";
       
       }
       public Doctors(String id,String name,String specialization,String degree) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.degree = degree;
       }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
    public ObservableList<Doctors> getDoctors(){
        ObservableList<Doctors> doctors = FXCollections.observableArrayList();
        return doctors;
       }

    public void initialize(){

       bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);
     //********************************************************************************** */
       TableColumn<Doctors,String> idColumn = new TableColumn<>("ID");
       idColumn.setMinWidth(200);
       idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
       TableColumn<Doctors,String> nameColumn = new TableColumn<>("Name");
       nameColumn.setMinWidth(400);
       nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
       TableColumn<Doctors,String> specializationColumn = new TableColumn<>("Specialization");
       specializationColumn.setMinWidth(200);
       specializationColumn.setCellValueFactory(new PropertyValueFactory<>("specialization"));
       TableColumn<Doctors,String> degreeColumn = new TableColumn<>("Degree");
       degreeColumn.setMinWidth(200);
       degreeColumn.setCellValueFactory(new PropertyValueFactory<>("degree"));

       idinput = new TextField();
       nameinput = new TextField();
       spinput = new TextField();
       degreeinput = new TextField();
       idinput.setPromptText("ID");
       nameinput.setPromptText("Name");
       spinput.setPromptText("Specialization");
       degreeinput.setPromptText("Degree");
       idinput.setPrefWidth(150);
       nameinput.setPrefWidth(350);
       spinput.setPrefWidth(150);
       degreeinput.setPrefWidth(150);
       add = new Button("Add");
       delete = new Button("Delete");
     

       add.setOnAction(e -> addButtonClicked());
       delete.setOnAction(e -> deleteButtonClicked());

       HBox inputHBox = new HBox(10,idinput,nameinput,spinput,degreeinput,add,delete);
       inputHBox.setAlignment(Pos.CENTER);

       table = new TableView<>();
       table.setItems(getDoctors());
       table.getColumns().addAll(idColumn,nameColumn,specializationColumn,degreeColumn);
       table.setLayoutX(600);
       table.setLayoutY(125);
       table.setPrefHeight(800);

       VBox tableVBox = new VBox(table,inputHBox);
       tableVBox.setPrefHeight(900);
       tableVBox.setPrefWidth(1000);
       tableVBox.setLayoutX(600);
       tableVBox.setLayoutY(100);
     //********************************************************************************** */

       Image backImage = new Image("images/backarrow.jpg");
       ImageView backImageView = new ImageView(backImage);
       backImageView.setFitHeight(40);
       backImageView.setFitWidth(40);
       backButton = new Button();
       backButton.setGraphic(backImageView);
       backButton.setLayoutX(1800);
       backButton.setLayoutY(980);
       backButton.setOnAction(new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            app1.NavigateToInfo();
        }
        
       });


       view = new Pane(tableVBox,backButton);
       view.setBackground(background);
       
    }

    public void addButtonClicked() {
        Doctors doctors = new Doctors();
        doctors.setId(idinput.getText());
        doctors.setName(nameinput.getText());
        doctors.setSpecialization(spinput.getText());
        doctors.setDegree(degreeinput.getText());
        table.getItems().add(doctors);
        idinput.clear();
        nameinput.clear();
        spinput.clear();
        degreeinput.clear();
    }
    public void deleteButtonClicked() {
        ObservableList<Doctors> doctorsSelected,allDoctors;
        allDoctors = table.getItems();
        doctorsSelected = table.getSelectionModel().getSelectedItems();
        doctorsSelected.forEach(allDoctors::remove);
    }
    
}
