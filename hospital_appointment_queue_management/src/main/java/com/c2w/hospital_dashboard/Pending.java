package com.c2w.hospital_dashboard;

import com.c2w.controller.AppNavigator2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class Pending {
    private AppNavigator2 app1;
    private Pane view;
    Image bgImage;
    BackgroundImage backgroundImage ;
    Button information_button;
    Button pending_appointment;
    Button completed_appointment;
    public Pending(AppNavigator2 app1) {
        this.app1 = app1;
        initialize();
    }
    public Pane getView() {
        return view;
    }
    public void initialize(){
        bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);
       
       information_button = new Button("Information");
       pending_appointment = new Button("Pending Appointments");
       completed_appointment = new Button("Completed Appointments");
       information_button.setPrefWidth(640);
       pending_appointment.setPrefWidth(640);
       completed_appointment.setPrefWidth(640);
       information_button.setPrefHeight(55);
       pending_appointment.setPrefHeight(55);
       completed_appointment.setPrefHeight(55);
       information_button.setStyle("-fx-font-weight:bold");
       pending_appointment.setStyle(" -fx-font-weight:bold");
       completed_appointment.setStyle("-fx-font-weight:bold");

       completed_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app1.NavigateToPrevious1();
        }
       });
       information_button.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app1.NavigateToInfo();
        }
       });
       
       HBox hBox = new HBox(information_button,pending_appointment,completed_appointment);
       hBox.setLayoutY(993);




       view = new Pane(hBox);
       view.setBackground(background);
    }

    
}
