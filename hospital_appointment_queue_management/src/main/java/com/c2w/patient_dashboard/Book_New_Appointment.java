package com.c2w.patient_dashboard;

import java.time.LocalDate;

import com.c2w.controller.AppNavigation;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


public class Book_New_Appointment {
    private AppNavigation app;
    private Pane view;
    Label hello ;
    Image bgImage;
    BackgroundImage backgroundImage ;
    Button book_new_appointment;
    Button upcoming_appointment;
    Button previous_appointment;
    Button availabel_doctor;
    Button availabel_appointment;
    Button ahead_appointment;
    Label fill_details;
    Label name;
    Label age;
    Label sex;
    Label issue;
    Label bloodgroup;
    TextField nameField;
    TextField ageField;
    TextField sexField;
    TextArea issueArea;
    TextField bloodField;
    Button confirm_appointment;
    Button logout;
    Button aboutus;

    public Book_New_Appointment(AppNavigation app) {
        this.app = app;
        initialize();
    }
    public Pane getView() {
        return view;
    }

    public void initialize() {

       hello = new Label("Hello Prashant...");
       hello.setLayoutY(15);
       hello.setLayoutX(5);
       hello.setFont(new Font("Cooper Black", 55));
       hello.setTextFill(Color.BLACK);
    
       bgImage = new Image("images/d3.jpg");
       backgroundImage = new BackgroundImage(bgImage,BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO,true,true,true, true));
       Background background = new Background(backgroundImage);
    
       book_new_appointment = new Button("Book New Appointment");
       upcoming_appointment = new Button("Upcoming Appointments");
       previous_appointment = new Button("Previous Appointments");
       book_new_appointment.setPrefWidth(640);
       upcoming_appointment.setPrefWidth(640);
       previous_appointment.setPrefWidth(640);
       book_new_appointment.setPrefHeight(55);
       upcoming_appointment.setPrefHeight(55);
       previous_appointment.setPrefHeight(55);
       book_new_appointment.setStyle("-fx-font-weight:bold");
       upcoming_appointment.setStyle(" -fx-font-weight:bold");
       previous_appointment.setStyle("-fx-font-weight:bold");
       
       HBox hBox = new HBox(book_new_appointment,upcoming_appointment,previous_appointment);
       hBox.setLayoutY(993);

       previous_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToPrevious();
        }  
       });
    upcoming_appointment.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            app.NavigateToUpcoming();
        }  
       });
       
       ChoiceBox<String> select_hospital = new ChoiceBox<>();
       select_hospital.getItems().addAll("Select Hospital","Shree Hospital","Saideep Hospital","Global hospital");
       select_hospital.setValue("Select Hospital");
       select_hospital.setPrefHeight(35);
       select_hospital.setPrefWidth(400);
       select_hospital.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
       
       ChoiceBox<String> select_time_slot = new ChoiceBox<>();
       select_time_slot.getItems().addAll("Select Time Slot");
       select_time_slot.setValue("Select Time Slot");
       select_time_slot.setPrefHeight(35);
       select_time_slot.setPrefWidth(175);
       select_time_slot.setStyle("-fx-background-color:Pink; -fx-font-weight:bold");

       DatePicker datePicker = new DatePicker();
       datePicker.setValue(LocalDate.now());
       datePicker.setPrefWidth(175);
       datePicker.setPrefHeight(35);
       datePicker.setStyle("-fx-font-weight:bold");

       
       availabel_doctor = new Button("Check Availabel Doctors");
       availabel_appointment = new Button("Check Availabel appointments in slot");
       ahead_appointment = new Button(" Check Appointments ahead of you in slot");
       availabel_doctor.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
       availabel_appointment.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
       ahead_appointment.setStyle("-fx-background-color:Pink ; -fx-font-weight:bold");
       
       
       availabel_doctor.setPrefHeight(35);
       availabel_appointment.setPrefHeight(35);
       ahead_appointment.setPrefHeight(35);
       
       fill_details = new Label("Fill Details : ");
       fill_details.setFont(new Font("Cooper Black", 35));
       name = new Label("Name");
       age = new Label("Age");
       sex = new Label("Sex");
       issue = new Label("Issue");
       bloodgroup = new Label("Blood Group");

       nameField = new TextField();
       nameField.setPrefWidth(345);
       nameField.setPrefHeight(35);
       ageField = new TextField();
       ageField.setPrefWidth(102);
       ageField.setPrefHeight(35);
       bloodField = new TextField();
       bloodField.setPrefWidth(102);
       bloodField.setPrefHeight(35);
       issueArea = new TextArea();
       HBox areaHBox = new HBox(issueArea);
       areaHBox.setPrefHeight(100);
        
        Label patientNumberLabel = new Label("Mobile Number");
        TextField patientNumberField = new TextField();
        patientNumberField.setPrefHeight(35);

        Label maritalStatusLabel = new Label("Marital Status");
        ComboBox<String> maritalStatusComboBox = new ComboBox<>();
        maritalStatusComboBox.getItems().addAll("Single", "Married", "Divorced", "Widowed", "Other");
        maritalStatusComboBox.setPrefHeight(35);
        
        HBox labelBox = new HBox(10,patientNumberLabel,patientNumberField);
        HBox labelBox2 = new HBox(24,maritalStatusLabel,maritalStatusComboBox);

        ToggleGroup sexToggleGroup = new ToggleGroup();
        RadioButton maleRadioButton = new RadioButton("Male");
        maleRadioButton.setToggleGroup(sexToggleGroup);
        RadioButton femaleRadioButton = new RadioButton("Female");
        femaleRadioButton.setToggleGroup(sexToggleGroup);
        RadioButton otherRadioButton = new RadioButton("Other");
        otherRadioButton.setToggleGroup(sexToggleGroup);

       HBox radioHBox = new HBox(5,maleRadioButton,femaleRadioButton,otherRadioButton);
       HBox sexHBox = new HBox(31,sex,radioHBox);
       
       confirm_appointment = new Button("Confirm Appointment");
       confirm_appointment.setPrefHeight(35);
       
       Button clearButton = new Button("Clear");
       clearButton.setPrefHeight(35);

       HBox clear_confirm = new HBox(10,confirm_appointment,clearButton);
       clear_confirm.setPrefWidth(345);
       clear_confirm.setAlignment(Pos.CENTER);

       HBox nameHBox = new HBox(15,name,nameField);
       HBox ageHBox = new HBox(28,age,ageField,bloodgroup,bloodField);
       VBox issueVBox = new VBox(issue);
       areaHBox.setPrefWidth(345);
       HBox issueHBox = new HBox(20,issueVBox,areaHBox);


       HBox date_time_hBox = new HBox(50,datePicker,select_time_slot);
       VBox drop_list_vBox = new VBox(30,select_hospital,date_time_hBox,availabel_doctor,availabel_appointment,ahead_appointment,fill_details,nameHBox,ageHBox,sexHBox,labelBox,labelBox2,issueHBox,clear_confirm);
       drop_list_vBox.setPrefWidth(400);
       drop_list_vBox.setAlignment(Pos.CENTER);
       drop_list_vBox.setLayoutY(105);
       drop_list_vBox.setLayoutX(1000);

       logout = new Button("Logout");
       logout.setPrefHeight(35);
       logout.setLayoutX(1845);
       logout.setLayoutY(5);
       aboutus = new Button("About Us");
       aboutus.setPrefHeight(35);
       aboutus.setLayoutX(1750);
       aboutus.setLayoutY(5);
       
    
       view = new Pane(hello,hBox,drop_list_vBox,logout,aboutus);
       view.setBackground(background);
       
    }


    
}
